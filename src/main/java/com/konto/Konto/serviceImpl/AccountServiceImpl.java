package com.konto.Konto.serviceImpl;
import com.konto.Konto.entity.AccountData;
import com.konto.Konto.service.AccountService;
import org.springframework.stereotype.Service;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

@Service
public class AccountServiceImpl implements AccountService {
    AccountData accountData = new AccountData();


    private List<AccountData> dataList = new ArrayList<>();

    HashMap<String,String> accounts = new HashMap<>();

    public List<AccountData> getAllData(){
        return dataList;
    }
    // personalId = 5
    // productType = 5
    // accountName = 6
    @Override
    public String createAndReturnAccountNumber(String personalId, String productType, String accountName) {
        accountData.setPersonalId(personalId);
        accountData.setProductType(productType);
        accountData.setAccountName(accountName);
        accountData.setAccountNumber(personalId + productType + accountName);

        String accountNumber = personalId + productType + accountName;

        accounts.put(personalId, accountNumber);

        dataList.add(accountData);

        return accountNumber;
    }

    public List<AccountData> showList(){
        return dataList;
    }

    @Override
    public String modifyAccountName(String accountName, String accountNumber) {

        accountNumber = accountNumber.substring(0,15) + accountData.getPersonalId() + accountData.getProductType() + accountName;
        return accountNumber;
    }

    @Override
    public AccountData fetchDetails(String accountNumber) {

        if (accountNumber!=null){
            System.out.println("Account name: " + accountData.getAccountName() +
                    "Personal id: " + accountData.getPersonalId() +
                    "Product type: " + accountData.getProductType());
        }
        return accountData;
    }

    @Override
    public void close(String accountNumber) {
        accountNumber = null;
    }
}
