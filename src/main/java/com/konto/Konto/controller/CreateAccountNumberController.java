package com.konto.Konto.controller;
import com.konto.Konto.entity.AccountData;
import com.konto.Konto.entity.Costumer;
import com.konto.Konto.serviceImpl.AccountServiceImpl;
import com.konto.Konto.serviceImpl.CostumerServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.validation.FieldError;
import org.springframework.web.bind.annotation.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import java.util.*;


@Controller
public class CreateAccountNumberController {

    Logger logger = LoggerFactory.getLogger(CreateAccountNumberController.class);
//
    @Autowired
    private AccountServiceImpl accountService;

    @Autowired
    private final CostumerServiceImpl costumerService;

    private static List<AccountData> accountDataList;

    private static List<Costumer> costumerList;

    private static Map<String,String> map;

    private final Random random;

    public CreateAccountNumberController(AccountServiceImpl accountService, CostumerServiceImpl costumerService) {
        this.accountService = accountService;
        this.costumerService = costumerService;
        this.random = new Random();
        accountDataList = Collections.synchronizedList(new ArrayList<>());
        map = new HashMap<>();
    }

    @GetMapping("data_form")
    public String showForm(Model model) {
        model.addAttribute("number", new AccountData());
        return "data_form";
    }
    @PostMapping("/data_form")
    public String formSubmit(@ModelAttribute AccountData accountData, Model model, BindingResult result) {
        if (map.containsKey(accountData.getPersonalId())){
//            FieldError error = new FieldError("number", "personalId", "id already exists");
//            result.addError(error);
            logger.info("Id already exists");
            return "data_form";
        }
            String accountNumber = accountService.createAndReturnAccountNumber(accountData.getPersonalId(), accountData.getProductType(), accountData.getAccountName());
            accountData.setAccountNumber((random.nextInt(44) + 10) + " " +
                    (random.nextInt(8999) + 1000) + " " +
                    (random.nextInt(8999) + 1000) + " " +
                    accountNumber);
            model.addAttribute("number", accountData);
            accountDataList.add(accountData);
            for (AccountData data : accountService.getAllData())
                map.put(data.getPersonalId(), data.getAccountNumber());
        return "redirect:/show_form";
    }
    @GetMapping("/show_form")
    public String getData(Model model){
        model.addAttribute("list", accountDataList);
        return "show_form";
    }

    @GetMapping("/edit")
    public String edit(Model model){
        model.addAttribute("numbers", map);
        logger.info(String.valueOf(accountDataList.size()));
        return "edit";
    }
    @PostMapping("/change_account_number")
    public String updateAccountNumber(Model model,@RequestParam(name = "accountName") String accountName,
                                      @RequestParam(name = "accountNumber") String accountNumber) {
        logger.info(accountName + " " + accountNumber);
        for(AccountData a: accountDataList) {
            logger.info("przed account number: " + accountNumber);
            String newAccountNumber = accountService.modifyAccountName(accountName, accountNumber);
            logger.info("nowy account number: " + newAccountNumber);
            map.put(a.getPersonalId(),newAccountNumber);
            for(int i=0; i<accountDataList.size();i++){
                if (a.getAccountNumber().equals(accountNumber))
                {
                    a.setAccountNumber(newAccountNumber);
                    accountDataList.set(i,new AccountData(a.getPersonalId(),a.getProductType(),a.getAccountName(),a.getAccountNumber()));
                }
            }
        }
        for(AccountData a: accountDataList){
            logger.info("Account values: " + a.getPersonalId()+ " " + a.getProductType() + " " + a.getAccountNumber() );
        }
        return "redirect:/edit";
    }

    @PostMapping("/close")
    public String closeBankAccount(@RequestParam(name = "number") String accountNumber){
        for(int i = 0; i<accountDataList.size();i++){
            if(accountDataList.get(i).getAccountNumber().equals(accountNumber))
                accountDataList.remove(accountDataList.get(i));
        }
        return "redirect:/show_form";
    }
}
