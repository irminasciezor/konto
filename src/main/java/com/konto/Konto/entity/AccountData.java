package com.konto.Konto.entity;

import java.io.Serializable;

public class AccountData implements Serializable {

    private String accountNumber;

    private String personalId;

    private String productType;

    private String accountName;

    public AccountData(String personalId, String productType, String accountName, String accountNumber) {
        this.personalId = personalId;
        this.productType = productType;
        this.accountName = accountName;
        this.accountNumber = accountNumber;
    }

    public AccountData() {
    }

    public String getAccountNumber() {
        return accountNumber;
    }

    public void setAccountNumber(String accountNumber) {
        this.accountNumber = accountNumber;
    }

    public String getPersonalId() {
        return personalId;
    }

    public void setPersonalId(String personalId) {
        this.personalId = personalId;
    }

    public String getProductType() {
        return productType;
    }

    public void setProductType(String productType) {
        this.productType = productType;
    }

    public String getAccountName() {
        return accountName;
    }

    public void setAccountName(String accountName) {
        this.accountName = accountName;
    }
}
