package com.konto.Konto.service;

import com.konto.Konto.entity.AccountData;

public interface AccountService {
    String createAndReturnAccountNumber(String personalId, String productType, String accountName);

    String modifyAccountName(String accountNumber, String accountName);

    AccountData fetchDetails(String accountNumber);

    void close(String accountNumber);
}
