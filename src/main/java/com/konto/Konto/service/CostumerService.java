package com.konto.Konto.service;

public interface CostumerService {
    boolean hasCostumerFile(String personalId, String firstName, String lastName);
}
